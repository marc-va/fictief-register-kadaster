package cmd

import "github.com/spf13/cobra"

var RootCmd = &cobra.Command{
	Use:   "api",
	Short: "Brp API",
}

func Execute() error {
	return RootCmd.Execute()
}

//nolint:gochecknoinits // this is the recommended way to use cobra
func init() {
	RootCmd.AddCommand(serveCommand)
	RootCmd.AddCommand(migrateCommand)
}
