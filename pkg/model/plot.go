package model

import "github.com/google/uuid"

type Plot struct {
	ID           uuid.UUID  `json:"id"`
	Municipality string     `json:"municipality"`
	Section      string     `json:"section"`
	Number       int32      `json:"number"`
	Surface      int32      `json:"surface"`
	Buildings    []Building `json:"buildings,omitempty"`
	Owners       []Person   `json:"owners,omitempty"`
}

type PlotPatch struct {
	ID           uuid.UUID `json:"id"`
	Municipality *string   `json:"municipality"`
	Section      *string   `json:"section"`
	Number       *int32    `json:"number"`
	Surface      *int32    `json:"surface"`
}

type Reparcelled struct {
	From []uuid.UUID `json:"from"`
	To   []PlotPatch `json:"to"`
}

type BuildOnPlot struct {
	BuildingID uuid.UUID `json:"buildingID"`
	PlotID     uuid.UUID `json:"plotID"`
}
