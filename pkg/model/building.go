package model

import (
	"time"

	"github.com/google/uuid"
)

type Building struct {
	ID            uuid.UUID  `json:"id"`
	ConstructedAt *time.Time `json:"constructedAt,omitempty"`
	Purpose       string     `json:"purpose,omitempty"`
	Surface       int32      `json:"surface,omitempty"`
	Plots         []Plot     `json:"plots,omitempty"`
}
