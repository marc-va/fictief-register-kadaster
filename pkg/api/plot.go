package api

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-kadaster/pkg/model"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-kadaster/pkg/storage/adapter"
)

func (a *API) PlotList(w http.ResponseWriter, r *http.Request) {
	records, err := a.db.Queries.PlotList(r.Context())
	if err != nil {
		writeError(w, err)
		return
	}

	plots, err := adapter.ToPlots(records)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(plots); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) PlotGet(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	record, err := a.db.Queries.PlotGet(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	ownerRecords, err := a.db.Queries.PeoplePlot(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	plot, err := adapter.ToPlot(record)
	if err != nil {
		writeError(w, err)
		return
	}

	if plot.Owners, err = adapter.ToPeople(ownerRecords); err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(plot); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (a *API) PlotCreate(w http.ResponseWriter, r *http.Request) {
	plot := &model.Plot{
		ID: uuid.New(),
	}
	if err := json.NewDecoder(r.Body).Decode(plot); err != nil {
		writeError(w, err)
		return
	}

	args := adapter.FromPlotCreate(plot)

	if err := a.db.Queries.PlotCreate(r.Context(), args); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (a *API) PlotUpdate(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "id"))
	if err != nil {
		writeError(w, err)
		return
	}

	plot := &model.Plot{}
	if err := json.NewDecoder(r.Body).Decode(plot); err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.PlotUpdate(r.Context(), adapter.FromPlotUpdate(id, plot)); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (a *API) Reparcelled(w http.ResponseWriter, r *http.Request) {
	reparcelled := &model.Reparcelled{}
	if err := json.NewDecoder(r.Body).Decode(reparcelled); err != nil {
		writeError(w, err)
		return
	}

	for idx := range reparcelled.To {
		if existsID(reparcelled.To[idx].ID, reparcelled.From) {
			// Do patch
			record, err := a.db.Queries.PlotGet(r.Context(), reparcelled.To[idx].ID)
			if err != nil {
				writeError(w, err)
				return
			}

			plot, err := adapter.ToPlot(record)
			if err != nil {
				writeError(w, err)
				return
			}

			args := adapter.FromPlotPatch(reparcelled.To[idx].ID, &reparcelled.To[idx], plot)
			if err := a.db.Queries.PlotUpdate(r.Context(), args); err != nil {
				writeError(w, err)
				return
			}

		} else {
			// Create the new plot
			args := adapter.FromPlotCreate(adapter.FromPlotPatchToCreate(reparcelled.To[idx].ID, &reparcelled.To[idx]))
			if err := a.db.Queries.PlotCreate(r.Context(), args); err != nil {
				writeError(w, err)
				return
			}
		}
	}

	w.WriteHeader(http.StatusNoContent)
}

// Check if an uuid exists in a given slice
func existsID(id uuid.UUID, ids []uuid.UUID) bool {
	for idx := range ids {
		if ids[idx] == id {
			return true
		}
	}
	return false
}

func (a *API) BuildOnPlot(w http.ResponseWriter, r *http.Request) {
	item := &model.BuildOnPlot{}
	if err := json.NewDecoder(r.Body).Decode(item); err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.BuildOnPlot(r.Context(), adapter.ToBuildOnPlot(item)); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (a *API) PlotPurchase(w http.ResponseWriter, r *http.Request) {
	personID, err := uuid.Parse(chi.URLParam(r, "personID"))
	if err != nil {
		writeError(w, err)
		return
	}

	plotID, err := uuid.Parse(chi.URLParam(r, "plotID"))
	if err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.PlotPurchase(r.Context(), adapter.ToPlotPurchase(personID, plotID)); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (a *API) PlotSell(w http.ResponseWriter, r *http.Request) {
	personID, err := uuid.Parse(chi.URLParam(r, "personID"))
	if err != nil {
		writeError(w, err)
		return
	}

	plotID, err := uuid.Parse(chi.URLParam(r, "plotID"))
	if err != nil {
		writeError(w, err)
		return
	}

	if err := a.db.Queries.PlotSell(r.Context(), adapter.ToPlotSell(personID, plotID)); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
}
