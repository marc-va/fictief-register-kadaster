package api

import (
	"net/http"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-kadaster/pkg/storage"
)

type API struct {
	*http.Server
	db *storage.Database
}

type NewAPIArgs struct {
	DB            *storage.Database
	ListenAddress string
}

func New(args *NewAPIArgs) (*API, error) {
	a := &API{
		db: args.DB,
	}

	a.Server = &http.Server{
		Addr:    args.ListenAddress,
		Handler: Router(a),
	}

	return a, nil
}

func (a *API) ListenAndServe() error {
	return a.Server.ListenAndServe()
}
