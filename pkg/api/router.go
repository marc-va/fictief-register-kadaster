package api

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func Router(a *API) *chi.Mux {
	r := chi.NewRouter()

	r.Use(Logger)
	r.Route("/plots", func(r chi.Router) {
		r.Get("/", a.PlotList)
		r.Post("/", a.PlotCreate)
		r.Post("/reparcelled", a.Reparcelled)

		r.Get("/{id}", a.PlotGet)
		r.Put("/{id}", a.PlotUpdate)
	})

	r.Route("/build-on-plot", func(r chi.Router) {
		r.Post("/", a.BuildOnPlot)
	})

	r.Route("/people", func(r chi.Router) {
		r.Route("/{personID}", func(r chi.Router) {
			r.Get("/", a.PeopleGet)
			r.Post("/plots/{plotID}/purchase", a.PlotPurchase)
			r.Post("/plots/{plotID}/sell", a.PlotPurchase)
		})
	})

	return r
}

func writeError(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func Logger(next http.Handler) http.Handler {
	return middleware.RequestLogger(&logFormatter{Logger: log.New(os.Stdout, "", log.LstdFlags)})(next)
}

type logFormatter struct {
	Logger middleware.LoggerInterface
}

type logEntry struct {
	Logger  middleware.LoggerInterface
	request *http.Request
	buf     *bytes.Buffer
}

func (l *logEntry) Write(status, bytes int, header http.Header, elapsed time.Duration, extra interface{}) {
	fmt.Fprintf(l.buf, "%03d %dB in %s", status, bytes, elapsed)
	l.Logger.Print(l.buf.String())
}

func (l *logEntry) Panic(v interface{}, stack []byte) {
	middleware.PrintPrettyStack(v)
}

func (l *logFormatter) NewLogEntry(r *http.Request) middleware.LogEntry {
	entry := &logEntry{
		Logger:  l.Logger,
		request: r,
		buf:     &bytes.Buffer{},
	}

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}

	fmt.Fprintf(entry.buf, "%s://%s%s %s\" ", scheme, r.Host, r.RequestURI, r.Proto)

	return entry
}
