package adapter

import (
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-kadaster/pkg/model"
)

func ToPerson(id uuid.UUID) (*model.Person, error) {
	return &model.Person{
		ID: id,
	}, nil
}

func ToPeople(records []uuid.UUID) ([]model.Person, error) {
	people := make([]model.Person, 0, len(records))

	for idx := range records {
		person, err := ToPerson(records[idx])
		if err != nil {
			return nil, err
		}

		people = append(people, *person)
	}

	return people, nil
}
