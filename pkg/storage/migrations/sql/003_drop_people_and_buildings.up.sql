BEGIN TRANSACTION;

ALTER TABLE digilab_demo_frk.private_owner_plot DROP CONSTRAINT fk_private_owner_plot_person_id;
ALTER TABLE digilab_demo_frk.building_plot DROP CONSTRAINT fk_building_plot_building_id_1;

DROP TABLE digilab_demo_frk.people;
DROP TABLE digilab_demo_frk.buildings;

COMMIT;   